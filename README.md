# x11grab

Desktop capture tool for Linux (tested on x11)

## Features

- Easy usage
- Capture all screen 
- Capture only a window, no problem if is not the top window
- Fast capture (video conversion is made on finish)
- Conversion using ffmpeg params

## Installation

Prerequisites: xdotool (optional) and maim

On Ubuntu:
```bash
sudo apt install -y xdotool maim
```

On OpenSuse
```bash
sudo zypper install xdotool maim
```


- Install [kwruntime/core](https://github.com/kwruntime/core)
- Execute on terminal: 

```bash 
kwrun --name=x11grab --install=gitlab://kodhework/x11grab@7228a43/main.ts
```


## Usage

```bash 
x11grab --capture --video=/path/to/generated.mkv
x11grab --capture --video=/path/to/generated.ts

# by default framerate is 20
x11grab --capture --video=/path/to/generated.ts --framerate=22 

# default options: "-pix_fmt", "yuv420p", "-vcodec", "libx264", "-preset", "fast", "-crf", "23"
x11grab --capture --video=/path/to/generated.mkv --ffmpeg-params="-pix_fmt yuv420p -vcodec libx265 -preset slow -crf 26"
x11grab --capture --video=/path/to/generated.mkv --ffmpeg-param="-pix_fmt" --ffmpeg-param="yuv420p" --ffmpeg-param="-vcodec" --ffmpeg-param="libx265" --ffmpeg-param="-preset" --ffmpeg-param="slow" --ffmpeg-param="-crf" --ffmpeg-param="26"

# capture only a window
x11grab --capture --window=WINDOW_ID --video=/path/to/generated.mp4
# select the window to capture (require xdotool)
x11grab --capture --select --video=/path/to/generated.mp4
```

## Contribution

Contributor are welcome. 