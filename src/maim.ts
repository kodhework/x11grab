
import Child, { ChildProcess } from 'child_process'
import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"
import { Grabber } from './grabber.ts'





export class Maim extends Grabber{

	name=  "maim"
	#state = {
		lastImage: Buffer.allocUnsafe(0),
		count: 0,
		frames: 0,
		int: null
	}
	

	async start(window: string | number, framerate: number = 20){
		this.#state.int = setInterval(()=>{
			if(this.#state.count > 4){
				if(this.#state.lastImage)
					this.#imagePut(this.#state.lastImage)
				return 
			}
			this.#getScreenshot(window)
		}, (1000 / framerate))
	}

	stop(){
		super.stop()
		if(this.#state.int) clearInterval(this.#state.int)
	}

	async #imagePut(image: Buffer){
		if(image.length){
			this.#state.frames++
			this.emit("data", image)
			this.emit("frame", this.#state.frames)
		}
		this.#state.lastImage = image
	}

	async #getScreenshot(window: string | number){
		this.#state.count ++
		try{
			let data = []
			let def = new async.Deferred<void>()
			let wparam = []
			if(window && (window != "root")) wparam = ["--window=" + String(window)]
			if(!this.options?.showcursor){
				wparam.push("-u")
			}
			let params = ["-f", "jpeg", "-m", "8",...wparam]
			let p = Child.spawn("maim", params)
			p.on("error", ()=> void(0))
			p.on("exit", def.resolve)
			p.stdout.on("data", (bytes)=> {
				data.push(bytes)
			})
			await def.promise 
			
			let image = Buffer.concat(data)     
			this.#imagePut(image)

		}catch(e){
			console.error("Failed getting image:", e)
		}
		this.#state.count --
	}

}

