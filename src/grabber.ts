import {AsyncEventEmitter} from "gh+/kwruntime/std@1.1.19/async/events.ts"
import {EventIterator}  from "gh+/kwruntime/std@1.1.19/async/event_iterator.ts"

export class Grabber extends AsyncEventEmitter{
	options: any 
	name: string 
	private $_iterator: EventIterator
	
	start(window: string | number, framerate?: number){}
	stop(){
		if(this.$_iterator)
			this.$_iterator.stop()
	}

	get iterator(){
		if(!this.$_iterator){
			this.$_iterator = this.getIterator(["frame", "data","error"])
		}
		return this.$_iterator
	}

}