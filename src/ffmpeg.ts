
import Child, { ChildProcess } from 'child_process'
import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"
import { Grabber } from './grabber.ts'



/*
export interface FfmpegOptions{
	vcodec?: string 
	format?: string 
	crf?: number 
}*/



export class FFMPEG extends Grabber{
	name = 'ffmpeg'
	#p: ChildProcess
	#options : {[key:string]: any}
	#iterator: EventIterator

	constructor(options: FfmpegOptions){
		super()
		this.#options = options || {}
	}
	
	get options(){
		return this.#options
	}

	set options(value){
		this.#options = value
	}
	
	async start(window: string | number, framerate: number = 20){
		let preset = []
		/*
		let defaultOptionsX264 = {
			"vcodec": "libx264",
			"f": "mjpeg",
			"preset": "ultasfast",
			"crf": 0
		}
		*/

		let defaultOptions = {
			"vcodec": "mjpeg",
			"f": "image2pipe",
			"q": 2
		}
		let ops:any = defaultOptions
		if(Object.keys(this.#options).length > 0){
			ops = this.#options
		}
		
		let optionArgs = []
		for(let entry of Object.entries(ops)){
			optionArgs.push("-" + entry[0])
			if(entry[1] !== undefined){
				optionArgs.push(String(entry[1]))
			}
		}
		window = window || 0
		let cmdargs = ["-r", String(framerate || 20), "-window_id", String(window), "-f", "x11grab", "-analyzeduration", "1", "-i", process.env.DISPLAY,
			...optionArgs,  "-"
		]
		this.#p = Child.spawn("ffmpeg", cmdargs)
		let def = new async.Deferred<void>()
		this.#p.once("error", def.reject)
		this.#p.once("exit", () => this.stop())
		this.#p.stdout.on("data", (bytes) => {
			this.emit("data", bytes)
		})
		this.#p.stderr.on("data", (bytes) =>{
			if(def){
				def.resolve()
				def = null
			}
			let str = bytes.toString()
			let reg = /frame\=\s*(\d+)\s*f/
			let match =  str.match(reg)
			if(match?.[1]){
				this.emit("frame", Number(match?.[1]))
			}
		})
		await def.promise 

	
	}

	stop(){
		if(this.#iterator) this.#iterator.stop()
		if(this.#p && !this.#p.killed){
			this.#p.kill()
		}
	}


	

}

