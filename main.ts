
import Child, { ChildProcess } from 'child_process'
import * as async from "gh+/kwruntime/std@1.1.18/util/async.ts"
import {Exception} from "gh+/kwruntime/std@1.1.18/util/exception.ts"

import fs from 'fs'
import Path from 'path'
import Os from 'os'
import {parse} from 'gitlab://jamesxt94/codes@e72b0bd/cli-params.ts'
import uniqid from "https://esm.sh/uniqid@5.4.0"
import { Writable } from 'stream'
import {terminal} from 'npm://terminal-kit@2.3.0'

import { Grabber } from './src/grabber'
import {Maim} from './src/maim.ts'
import {FFMPEG} from './src/ffmpeg.ts'


let userData = Path.join(Os.homedir(), ".kawi", "user-data")
if(!fs.existsSync(userData)) fs.mkdirSync(userData)
userData = Path.join(userData, "com.kodhe.screengrab")
if(!fs.existsSync(userData)) fs.mkdirSync(userData)


export interface VideoParams{
	framerate?: number 
	format?: string 
	ffmpegParams?: string[]
}

export interface CaptureOptions{
	file?: string 
	window: string | number 
	framerate?: number 
	capturer?: {
		name: string 
		options?: any 
	}
}



export class Program{

	#state = {
		lastImage: Buffer.allocUnsafe(0),
		count: 0,
		frames: 0,
		int: null
	}
	#grab: Grabber
	#p: ChildProcess
	silent = false
	
	#date_start: number = 0
	#stream: Writable

	get grab(){
		return this.#grab
	}	

	async $getStd(cmd, args, options = undefined){
		let response = {
			stdout: [],
			stderr: []
		}
		let p = Child.spawn(cmd, args, options)
		let def = new async.Deferred<void>()
		p.on("exit", def.resolve)
		p.on("error", def.reject)
		p.stdout.on("data", function(b){
			response.stdout.push(b)
		})
		p.stderr.on("data", function(b){
			response.stderr.push(b)
		})
		await def.promise
		return response
	}

	async capture(options: CaptureOptions){
		
		if(options.file)
			this.#stream = fs.createWriteStream(options.file)
		let grab: Grabber
		let capturer = options.capturer?.name || "maim"
		if(capturer == "maim"){
			grab = new Maim(options.capturer.options)
		}
		if(capturer == "ffmpeg"){
			grab = new FFMPEG(options.capturer.options)
		}
		this.#grab = grab
		await grab.start(options.window, options.framerate)
		for await (let event of grab.iterator){
			if(event.type == "data"){
				this.#imagePut(event.data)
			}
			else if(event.type == "frame"){
				if(!this.silent)
					this.#writeFrames(event.data)
			}
		}

	}

	#writeFrames(frames: number){
		if(this.#state.frames > 0){
			terminal.up()
		}
		this.#state.frames = frames
		console.info("> Getting images, frames=", this.#state.frames)
	}

	async #imagePut(image: Buffer){
		if(!this.#date_start){
			this.#date_start = Date.now()
		}
		if(this.#stream?.writable && image.length){
			this.#stream.write(image)
		}
		this.#state.lastImage = image
	}

	


	async convert(src: string, dest: string, options: VideoParams = {}){
		let def = new async.Deferred<void>()
		let time = 0, er = []
		let formatarr = []
		if(options.format) formatarr.push("-f", options.format)

		let ffmpegParams = []
		if(!options.format || (["mp4","matroska","mpegts"].indexOf(options.format) >= 0)){
			ffmpegParams .push( "-pix_fmt", "yuv420p", "-vcodec", "libx264", "-preset", "ultrafast", "-crf", "22")
		}

		if(options.ffmpegParams){
			ffmpegParams = options.ffmpegParams
		}
		let inargs = ["-f", "image2pipe", "-r", String(options.framerate || 20)]
		if(this.#grab.name == "ffmpeg"){
			if(this.#grab.options.format?.indexOf("image") >= 0){
				//pass
			}
			else{
				inargs = ["-r", String(options.framerate || 20)]
			}
		}

		let cmdargs = [
			"-y", ...inargs,
			"-i", src,
			"-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2",
			...ffmpegParams,
			...formatarr,
			dest]
		let p = this.#p = Child.spawn("ffmpeg", cmdargs)
		//console.info(cmdargs)


		p.on("error", ()=> void(0))
		p.on("exit", def.resolve)
		p.stderr.on("data", (bytes)=>{
			let reg = /frame\=\s*(\d+)\s*f/
			let match = bytes.toString().match(reg)
			if(match){

				if(!this.silent){
					if(time > 0){
						terminal.up()
					}
					console.info("> Writing video, frames=", match[1])
				}
				
				time++ 
				//let text = bytes.toString()  
			}else{
				let lines = bytes.toString().split("\n")
				for(let line of lines){
					line = line?.trim()
					if(line){
						if(line.toLowerCase().indexOf("error") >= 0){
							if(!line.startsWith("configuration:"))
								er.push(line)
						}
					}
				}
				
			}
		})
		await def.promise 
		if(time <= 0){
			throw Exception.create("FFMPEG error: " + (er.join("\n") || "no data processed or codec configuration error")).putCode("FFMPEG_ERROR")
		}
	}


	async selectWindow(){
		let resp = await this.$getStd("xdotool", ["selectwindow"])
		if(resp.stderr.length){
			let er = Buffer.concat(resp.stderr).toString()
			throw Exception.create(er).putCode("WINDOW_SELECT_ERROR")
		}
		return  Number(Buffer.concat(resp.stdout).toString().trim())
	}

	async startCapture(params: {[key: string]: any}){


		let capturer = {
			name: params.method || 'maim',
			options: {}
		}

		for(let param of Object.entries(params)){
			if(param[0].startsWith(capturer.name + "-")){
				let name = param[0].substring(capturer.name.length + 1)
				capturer.options[name] = param[1]
			}
		}
		return await this.capture({
			capturer,
			file: params.out,
			window: params.window,
			framerate: params.framerate
		})
	}

	async main(){
		let cli = parse()
		if(cli.params.convert !== undefined){
			await this.convert(cli.params.in, cli.params.out, cli.params)
		}
		if(cli.params.capture !== undefined){
			let deletefile = false
			if(cli.params.select !== undefined){
				// select window id
				console.info("> Please select the window to capture")
				cli.params.window = await this.selectWindow()

				if(!this.silent)
					console.info("> Using window:", cli.params.window)
			}
			// start grab 
			if(!cli.params.out){
				cli.params.out = Path.join(userData, uniqid() + ".jpegdata")
				deletefile = true
			}

			let converting = false
			let finalAction = async ()=>{
				if(deletefile){
					await fs.promises.unlink(cli.params.out)
				}
			}
			let converttovideo = async ()=>{
				if(converting) return 

				converting = true 
				try{
					
					this.#grab.stop()

					if(cli.params.video){
						console.info("> Converting to video. Press 'q' again to cancel the conversion")
						let videoParams = Object.assign({}, cli.params)
						if(cli.params["convert-params"]){
							videoParams.ffmpegParams = cli.params["convert-params"].split(" ")
						}
						if(cli.paramsArray["convert-param"]){
							videoParams.ffmpegParams = videoParams.ffmpegParams || []
							videoParams.ffmpegParams.push(...cli.paramsArray["convert-param"])
						}
						await this.convert(cli.params.out, cli.params.video, videoParams)
					}
				}catch(e){
					throw e 
				}
				finally{
					//await finalAction()
				}
			}
			process.on("SIGINT", async function(){
				await converttovideo()
				this.#grab.stop()
				if(this.#p && !this.#p.killed){
					this.#p.kill('SIGKILL')
				}
				await finalAction()
				process.exit(0)
			})

			process.stdin.setRawMode(true)
			process.stdin.on("data", async (b)=>{
				let text= b.toString()                
				if(text == "q"){
					this.#grab.stop()
					await converttovideo()
					if(this.#p){
						try{
							this.#p.kill('SIGKILL')
						}catch(e){}
					}
					await finalAction()
					process.exit(0)
				}
			})
			process.stdin.resume()
			console.info("> Capturing screen/window. Press 'q' to finish")

			return await this.startCapture(cli.params)
		}
	}

	static async main(){
		try{            
			await new Program().main()
		}catch(e){
			console.error("Failed execute:", e.message)
			process.exit(1)
		}
	}
}

